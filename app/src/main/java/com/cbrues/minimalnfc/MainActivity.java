// ----------------------------------------------------------------------------
// "THE BEER-WARE LICENSE" (Revision 42):
// <cbrues@gmail.com> wrote this file.  As long as you retain this notice you
// can do whatever you want with this stuff. If we meet some day, and you think
// this stuff is worth it, you can buy me a beer in return.   Cody Browne
// ----------------------------------------------------------------------------
package com.cbrues.minimalnfc;

import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Build;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity{

	@Override
	protected void onCreate( Bundle savedInstanceState ){
		super.onCreate( savedInstanceState );
		setContentView( R.layout.activity_main );

		// Setup the foreground dispatch intent
		mforegroundDispatchIntent = PendingIntent.getActivity(
			this
			, 0
			, new Intent( this, getClass() ).addFlags( Intent.FLAG_ACTIVITY_SINGLE_TOP )
			, 0
		);

		initNfc();

		// Handle launch intent
		handleIntent( getIntent() );
	}

	@Override
	protected void onResume(){
		super.onResume();
		initNfc();
		NfcAdapter.getDefaultAdapter( this ).enableForegroundDispatch(
			this
			, mforegroundDispatchIntent
			, null
			, null
		);
	}

	@Override
	protected void onPause(){
		super.onPause();
		mNfcAdapter.disableForegroundDispatch( this );
	}

	@Override
	protected void onNewIntent( Intent intent ){
		super.onNewIntent( intent );
		handleIntent( intent );
	}

	private void handleIntent( Intent intent ){
		String action = intent.getAction();
		if(
			NfcAdapter.ACTION_NDEF_DISCOVERED.equals( action )
				|| NfcAdapter.ACTION_TECH_DISCOVERED.equals( action )
				|| NfcAdapter.ACTION_TAG_DISCOVERED.equals( action )
			){
			Tag tag = intent.getParcelableExtra( NfcAdapter.EXTRA_TAG );
			StringBuilder sb = new StringBuilder();
			for( byte b : tag.getId() ){
				sb.append( String.format( "%02X", b ) );
			}
			Log.d( kLogTag , sb.toString() );
		}
	}

	private void initNfc(){
		Log.d( kLogTag, "Setting up NFC" );
		mNfcAdapter = NfcAdapter.getDefaultAdapter( this );
		if( mNfcAdapter == null ){
			AlertDialog.Builder builder = new AlertDialog.Builder( this );
			builder.setMessage( "NFC is not supported" );
			builder.create().show();
		}
		else{
			if( !mNfcAdapter.isEnabled() ){
				AlertDialog.Builder builder = new AlertDialog.Builder( this );
				builder.setMessage( "NFC is disabled. Do you want to go to settings and enable it?" )
					.setPositiveButton( android.R.string.yes, new DialogInterface.OnClickListener(){
						@Override
						public void onClick( DialogInterface dialog, int which ){
							dialog.dismiss();
							Intent intent;
							if( Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN ){
								intent = new Intent( Settings.ACTION_NFC_SETTINGS );
							}
							else{
								intent = new Intent( Settings.ACTION_WIRELESS_SETTINGS );
							}
							startActivity( intent );
						}
					} )
					.setNegativeButton( android.R.string.no, new DialogInterface.OnClickListener(){
						@Override
						public void onClick( DialogInterface dialog, int which ){
							dialog.dismiss();
						}
					} )
					.create()
					.show()
				;
			}
		}
	}

	private static final String kLogTag = "MinimalNFC";

	private PendingIntent mforegroundDispatchIntent;
	private NfcAdapter mNfcAdapter;
}
